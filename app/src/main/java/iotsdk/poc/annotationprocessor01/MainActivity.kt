package iotsdk.poc.annotationprocessor01

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import iotsdk.poc.api.inject
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.IllegalStateException
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var injectable: Injectable

    @Inject
    lateinit var injectable2: Injectable

    @Inject
    lateinit var presenter: Presenter

    @Inject
    lateinit var presenter2: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject(this)
        setContentView(R.layout.activity_main)

        textView.text = injectable.name()
        injectable == injectable2 || throw IllegalStateException("The 2 Injectable must be the same")
        presenter != presenter2 || throw IllegalStateException("The 2 Presenter must NOT be the same")
    }
}
