package iotsdk.poc.annotationprocessor01

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Injectable
@Inject constructor() {
    fun name(): String = "Je suis l'Injectable"
}