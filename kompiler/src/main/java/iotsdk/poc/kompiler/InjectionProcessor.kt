package iotsdk.poc.kompiler

import iotsdk.poc.kompiler.generator.GlobalInjectorGenerator
import iotsdk.poc.kompiler.generator.InjectorGenerator
import iotsdk.poc.kompiler.generator.ProviderGenerator
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Messager
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.annotation.processing.SupportedAnnotationTypes
import javax.annotation.processing.SupportedSourceVersion
import javax.inject.Inject
import javax.lang.model.SourceVersion
import javax.lang.model.element.ElementKind
import javax.lang.model.element.TypeElement
import javax.tools.Diagnostic

@SupportedAnnotationTypes("javax.inject.Inject")
@SupportedSourceVersion(SourceVersion.RELEASE_6)
class InjectionProcessor : AbstractProcessor() {

    private lateinit var messager: Messager
    private lateinit var output: File
    private lateinit var providerGenerator: ProviderGenerator
    private lateinit var injectorGenerator: InjectorGenerator
    private lateinit var globalInjectorGenerator: GlobalInjectorGenerator

    override fun init(environment: ProcessingEnvironment) {
        super.init(environment)
        messager = environment.messager
        output = File(processingEnv.options["kapt.kotlin.generated"])
        providerGenerator = ProviderGenerator(messager, output)
        injectorGenerator = InjectorGenerator(messager, output)
        globalInjectorGenerator = GlobalInjectorGenerator(messager, output)
    }

    override fun process(set: MutableSet<out TypeElement>, roundEnv: RoundEnvironment): Boolean {
        messager.printMessage(Diagnostic.Kind.WARNING, "Proccessing the processor")
        val count = roundEnv.getElementsAnnotatedWith(Inject::class.java)
                .asSequence()
                .filter { element -> element.kind == ElementKind.CONSTRUCTOR }
                .map { element -> element.enclosingElement }
                .onEach(providerGenerator::generateFactory)
                .count()

        if (count == 0) {
            return false
        }

        val injectFunctions = roundEnv.getElementsAnnotatedWith(Inject::class.java)
                .asSequence()
                .filter { element -> element.kind == ElementKind.FIELD }
                .map { element -> element.enclosingElement }
                .distinct()
                .onEach(injectorGenerator::generateInjector)
                .map(globalInjectorGenerator::generateInjectFunction)

        globalInjectorGenerator.generateGlobalInjectionFile(injectFunctions)

        return false
    }
}
