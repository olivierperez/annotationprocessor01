package iotsdk.poc.kompiler

import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.PackageElement

val Element.packageName: String
    get() {
        var e = this
        while (e.kind != ElementKind.PACKAGE) {
            e = e.enclosingElement
        }
        return (e as PackageElement).toString()
    }