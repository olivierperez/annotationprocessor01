package iotsdk.poc.kompiler.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.asTypeName
import iotsdk.poc.kompiler.packageName
import java.io.File
import javax.annotation.processing.Messager
import javax.lang.model.element.Element
import javax.tools.Diagnostic

class GlobalInjectorGenerator(val messager: Messager, val output: File) {
    fun generateInjectFunction(element: Element): FunSpec {
        val elementType = element.asType().asTypeName()
        val elementTypeName = element.simpleName.toString()
        messager.printMessage(Diagnostic.Kind.WARNING, "pack: ${element.packageName}")
        return FunSpec.builder("inject")
                .addModifiers(KModifier.INLINE)
                .addParameter("instance", elementType)
                .addStatement("%L().inject(instance)", ClassName(element.packageName, "${elementTypeName}_Injector"))
                .build()
    }

    fun generateGlobalInjectionFile(injectFunctions: Sequence<FunSpec>) {
        FileSpec.builder("iotsdk.poc.api", "InjectorExt")
                .addComment("Generated by Olivier, just for you ;-)")
                .apply {
                    injectFunctions.forEach {
                        addFunction(it)
                    }
                }
                .build()
                .writeTo(output)
    }
}